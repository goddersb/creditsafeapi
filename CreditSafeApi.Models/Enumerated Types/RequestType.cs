﻿namespace CreditSafeApi.Models.Enumerated_Types
{
    public static class RequestType
    {
        public const string Individual = "Individual";
        public const string Business = "Business";
        public const string Bank = "Bank";
    }
}
