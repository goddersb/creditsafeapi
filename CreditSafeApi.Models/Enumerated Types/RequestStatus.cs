﻿namespace CreditSafeApi.Models.Enumerated_Types
{
    public static class RequestStatus
    {
        public const string Completed = "Completed";
        public const string Failed = "Failed";
    }
}
