﻿namespace CreditSafeApi.Models.Configuration
{
    public class ExchangeConfig
    {
        public string ExchangeEmailPath { get; set; }
        public string ExhangePassword { get; set; }
        public string ExchangeAddress { get; set; }
        public string TenantId { get; set; }
        public string AppId { get; set; }
        public string ClientSecret { get; set; }
        public string EncryptionKey { get; set; }
    }
}