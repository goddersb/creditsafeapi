﻿namespace CreditSafeApi.Models.Configuration
{
    public class CreditSafeConfig
    {
        public string DolfinPaymentConnectionString { get; set; }
        public string PersonApiKey { get; set; }
        public string BusinessApiKey { get; set; }
        public string CountriesApiKey { get; set; }
        public string RiskAssessmentPersonStoredProcedure { get; set; }
        public string RiskAssessmentBusinessStoredProcedure { get; set; }
        public string FetchCountriesStoredProcedure { get; set; }
        public string FetchFilePathsStoredProcedure { get; set; }
        public string FetchClientDetailsStoredProcedure { get; set; }
        public string FetchClientValidateStoredProcedure { get; set; }
        public string FetchClientDetailsByEmailStoredProcedure { get; set; }
        public string FetchApiKeyStoredProcedure { get; set; }
        public string FetchAdminApiKeyStoredProcedure { get; set; }
        public string UpdateClientDetailsStoredProcedure { get; set; }
        public string InsertClientRequestsStoredProcedure { get; set; }
        public string InsertClientDetailsStoredProcedure { get; set; }
        public string FetchAdminIdStoredProcedure { get; set; }
        public string InsertClientAuthStoredProcedure { get; set; }
        public string ValidateClientAuthStoredProcedure { get; set; }
        public string UpdateClientAuthStoredProcedure { get; set; }
        public string VerificationLinkExpiredStoredProcedure { get; set; }
        public string DefaultToken { get; set; }
        public int DefaultSwiftAllocation { get; set; }
        public int DefaultSearchAllocation { get; set; }
        public string InsertFtpFilesStoredProcedure { get; set; }
        public string SwiftApiUrl { get; set; }
        public string RedirectUrl { get; set; }
        public string BccAddress { get; set; }
    }
}