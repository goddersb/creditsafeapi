﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditSafeApi.Models.Api
{
    /// <summary>
    /// The reponse model for the returned JSON.
    /// </summary>
    public class PersonResponse
    {
        public int score { get; set; }
        public Person person { get; set; }
        public class Person
        {
            public string id { get; set; }
            public Title? title { get; set; }
            public class Title
            {
                public string description { get; set; }
            }
            public string alternativeTitle { get; set; }
            public string forename { get; set; }
            public string middlename { get; set; }
            public string surname { get; set; }
            public DateTime? dateOfBirth { get; set; }
            public string? yearOfBirth { get; set; }
            public DateTime? dateOfDeath { get; set; }
            public string? yearOfDeath { get; set; }
            public bool? isDeceased { get; set; }
            public string gender { get; set; }
            public Nationality nationality { get; set; }
            public class Nationality
            {
                public string nationality { get; set; }
            }
            public string? imageURL { get; set; }
            public string telephoneNumber { get; set; }
            public string faxNumber { get; set; }
            public string mobileNumber { get; set; }
            public string email { get; set; }
            public string? pepLevel { get; set; }
            public bool? isPEP { get; set; }
            public bool? isSanctionsCurrent { get; set; }
            public bool? isSanctionsPrevious { get; set; }
            public bool? isLawEnforcement { get; set; }
            public bool? isFinancialregulator { get; set; }
            public bool? isDisqualifiedDirector { get; set; }
            public bool? isInsolvent { get; set; }
            public bool? isAdverseMedia { get; set; }
            public Addresses[] addresses { get; set; }
            public class Addresses
            {
                public string address1 { get; set; }
                public string address2 { get; set; }
                public string address3 { get; set; }
                public string address4 { get; set; }
                public string city { get; set; }
                public string county { get; set; }
                public string postcode { get; set; }
                public Country country { get; set; }
                public class Country
                {
                    public string name { get; set; }
                }
            }
            public Aliases[] aliases { get; set; }
            public class Aliases
            {
                public string title { get; set; }
                public string alternativeTitle { get; set; }
                public string forename { get; set; }
                public string middlename { get; set; }
                public string surname { get; set; }
                public string businessName { get; set; }
            }
            public Articles[]? articles { get; set; }
            public class Articles
            {
                public string originalURL { get; set; }
                public string? dateCollected { get; set; }
                public string c6URL { get; set; }
                public Categories[] categories { get; set; }
                public class Categories
                {
                    public string name { get; set; }
                }
            }
            public Sanctions[]? sanctions { get; set; }
            public class Sanctions
            {
                public string isCurrent { get; set; }
                public SanctionType sanctionType { get; set; }
                public class SanctionType
                {
                    public string description { get; set; }
                }
            }
            public Notes[] notes { get; set; }
            public class Notes
            {
                public string text { get; set; }
                public DataSource dataSource { get; set; }
                public class DataSource
                {
                    public string name { get; set; }
                }
            }
            public LinkedBusinesses[] linkedBusinesses { get; set; }
            public class LinkedBusinesses
            {
                public int? businessId { get; set; }
                public string businessName { get; set; }
                public string position { get; set; }
            }
            public LinkedPersons[] linkedPersons { get; set; }
            public class LinkedPersons
            {
                public int? personId { get; set; }
                public string name { get; set; }
                public string association { get; set; }
            }
            public PoliticalPositions[] politicalPositions { get; set; }
            public class PoliticalPositions
            {
                public string description { get; set; }
                public string? from { get; set; }
                public string? to { get; set; }
                public Country country { get; set; }
                public class Country
                {
                    public string name { get; set; }
                }
            }
        }
    }
}
