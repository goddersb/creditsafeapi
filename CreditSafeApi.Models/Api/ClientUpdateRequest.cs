﻿using System;

namespace CreditSafeApi.Models.Api
{
    public class ClientUpdateRequest
    {
        public string Title { set; get; }
        public string Forename { set; get; }
        public string Middlename { set; get; }
        public string Surname { set; get; }
        public string CompanyName { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string updatedBy { get; set; }
    }
}