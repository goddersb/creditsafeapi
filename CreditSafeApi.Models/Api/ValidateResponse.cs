﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditSafeApi.Models.Api
{
    public class ValidateResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
    }
}
