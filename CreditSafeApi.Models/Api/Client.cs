﻿using System;

namespace CreditSafeApi.Models.Api
{
    public class Client
    {
        public int ClientId { get; set; }
        public string Title { set; get; }
        public string Forename { set; get; }
        public string Middlename { set; get; }
        public string Surname { set; get; }
        public string CompanyName { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string? ApiKey { get; set; }
        public bool Active { get; set; }
        public DateTime? EndDate { get; set; }
        public int SearchRequestsAvailable { get; set; }
        public int SearchRequestsAllocated { get; set; }
        public int SearchRequestsUsed { get; set; }
        public int SwiftRequestsAvailable { get; set; }
        public int SwiftRequestsAllocated { get; set; }
        public int SwiftRequestsUsed { get; set; }
    }
}