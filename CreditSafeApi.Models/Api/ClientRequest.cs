﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditSafeApi.Models.Api
{
    public class ClientRequest
    {
        public string Title { set; get; }
        public string Forename { set; get; }
        public string Middlename { set; get; }
        public string Surname { set; get; }
        public string CompanyName { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        //public DateTime? EndDate { get; set; }
        public int? SearchAllocation { get; set; }
        public int? SwiftAllocation { get; set; }

    }
}
