﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditSafeApi.Models.Api
{
    public class ResendVerificationRequest
    {
        public string Email { get; set; }
    }
}
