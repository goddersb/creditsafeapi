﻿using System;

namespace CreditSafeApi.Models.Api
{
    public class ClientDetails
    {
        public string Title { set; get; }
        public string ClientId { get; set; }
        public string Name { set; get; }
        public string CompanyName { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public int TypeId { get; set; }
        public int SearchRequestsAvailable { get; set; }
        public bool Payments { get; set; }
        public bool FOP { get; set; }
        public bool Orders { get; set; }
        public bool Settlement { get; set; }
        public bool CorporateActions { get; set; }
        public bool SystemReconciliation { get; set; }
        public bool Swift { get; set; }
        public bool Active { get; set; }
        public int SearchRequestsAllocated { get; set; }
        public int SearchRequestsUsed { get; set; }
        public int SwiftRequestsAvailable { get; set; }
        public int SwiftRequestsAllocated { get; set; }
        public int SwiftRequestsUsed { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? AuthDate { get; set; }
    }
}