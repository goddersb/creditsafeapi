﻿namespace CreditSafeApi.Models.Api
{
    public class SearchResult
    {
        public string JsonResponse { get; set; }
        public int ResponseCode { get; set; }
        public string Message { get; set; }
        public bool ValidResponse { get; set; }
    }
}
