﻿using CreditSafeApi.Models.Api;
using CreditSafeApi.Models.Configuration;
using CreditSafeApi.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CreditSafeApi.Repositories
{
    public class GenericFunctionsRepository : IGenericFunctions
    {
        private readonly CreditSafeConfig _creditSafeConfig;
        private readonly NrecoConfig _nrecoConfig;
        private readonly ILogger _logger;
        private const string initVector = "tu89geji340t89u2";
        private const int keysize = 256;

        public GenericFunctionsRepository(CreditSafeConfig creditSafeConfig, NrecoConfig nrecoConfig, ILogger logger)
        {
            _creditSafeConfig = creditSafeConfig;
            _nrecoConfig = nrecoConfig;
            _logger = logger;
        }

        /// <summary>
        /// Check key exists in the dictionary object.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool CheckKeyExists(Dictionary<string, object> result, string key)
        {
            if (result.ContainsKey(key))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Extract value from dictionary by key.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetValueFromDictionary(Dictionary<string, object> result, string key)
        {
            if (result.ContainsKey(key))
            {
                var value = (object)result.FirstOrDefault(pair => pair.Key == key).Value;
                return value.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Extract value from dictionary by key.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetValueFromDictionary(Dictionary<string, string> result, string key)
        {
            if (result.ContainsKey(key))
            {
                var value = (object)result.FirstOrDefault(pair => pair.Key == key).Value;
                return value.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Check to see if the directory exists for the path passed to it.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string CheckforDirectory(string filepath, double clientId)
        {
            try
            {
                var directories = Directory.GetDirectories(filepath, $"*{clientId}");
                if (directories.Length > 0)
                {
                    return directories[0].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error searching for directory ClientId={ClientId}.", clientId);
                return string.Empty;
            }
        }

        /// <summary>
        /// Generate a PDF file from the HTML string.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="clientId"></param>
        /// <param name="filePath"></param>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public async Task<string> GeneratePdfFile(string type, double clientId, string filePath, string htmlString, string clientName, bool ftp, string RequestName)
        {
            try
            {
                var directory = CheckforDirectory($"{filePath}PDF\\{clientId}", clientId);
                if (string.IsNullOrEmpty(directory))
                {
                    Directory.CreateDirectory($"{filePath}PDF\\{clientId}");
                    directory = $"{filePath}PDF\\{clientId}";
                }

                //Build the Pdf file path.
                string pdfDoc = $"{directory}\\{type}_Search_{clientId}_{RequestName}_{String.Format("{0:ddMMyyyy_HHmm}", DateTime.Now)}.pdf";

                var htmlToPdfConv = new NReco.PdfGenerator.HtmlToPdfConverter();
                htmlToPdfConv.License.SetLicenseKey(_nrecoConfig.NrecoId, _nrecoConfig.NrecoKey);
                htmlToPdfConv.PdfToolPath = _nrecoConfig.NrecoToolPath;
                htmlToPdfConv.PageHeaderHtml = $"<html><head></head><body><center><span style ='font-size:medium;font-family:\"Segoe UI\",\"sans-serif\";color:#172B4D'>{type} Search Report for {clientName} - Client Id {clientId}, report created at {DateTime.Now.ToString("dd/MM/yyyy HH:mm tt")}</span></center></body></html>"; 
                htmlToPdfConv.PageFooterHtml = $"<center><span style ='font-size:9.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:#172B4D'> Page <span class=\"page\"></span> of <span class=\"topage\"></span></span></center>";
                htmlToPdfConv.GeneratePdf(htmlString, null, pdfDoc);

                //For Ftp requests we add the details to the table.
                if(ftp)
                {
                    var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);
                    await sqlRepository.InsertFtpFiles(clientId, pdfDoc);
                }

                _logger.LogInformation("PDF file {} created successfully. ClientId={ClientId}", pdfDoc, clientId);

                return pdfDoc;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error creating html file. ClientId={ClientId}", clientId);
                return string.Empty;
            }
        }
        public string GenerateEncryptionKey() 
        {

            Aes aes = Aes.Create();
            aes.GenerateIV();
            aes.GenerateKey();

            return Convert.ToBase64String(aes.Key);
        }

        public string Encrypt(string Text, string Key)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(Text);
            PasswordDeriveBytes password = new PasswordDeriveBytes(Key, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] Encrypted = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(Encrypted);
        }

        public string Decrypt(string EncryptedText, string Key)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] DeEncryptedText = Convert.FromBase64String(EncryptedText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(Key, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(DeEncryptedText);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[DeEncryptedText.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }
    }
}