﻿using CreditSafeApi.Models.Api;
using CreditSafeApi.Models.Configuration;
using CreditSafeApi.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Globalization;

namespace CreditSafeApi.Repositories
{
    public class TablesRepository : ITables
    {
        private readonly CreditSafeConfig _creditSafeConfig;
        private readonly ILogger _logger;

        public TablesRepository(CreditSafeConfig creditSafeConfig, ILogger logger)
        {
            _creditSafeConfig = creditSafeConfig;
            _logger = logger;
        }

        public void CreatePersonColumns(DataTable tblPerson) 
        {
            try
            {
                AddDataColumn(tblPerson, "score", "System.Int32");
                AddDataColumn(tblPerson, "id", "System.String");
                AddDataColumn(tblPerson, "title", "System.String");
                AddDataColumn(tblPerson, "alternativeTitle", "System.String");
                AddDataColumn(tblPerson, "forename", "System.String");
                AddDataColumn(tblPerson, "middlename", "System.String");
                AddDataColumn(tblPerson, "surname", "System.String");
                AddDataColumn(tblPerson, "dateOfBirth", "System.DateTime");
                AddDataColumn(tblPerson, "yearOfBirth", "System.String");
                AddDataColumn(tblPerson, "dateOfDeath", "System.DateTime");
                AddDataColumn(tblPerson, "yearOfDeath", "System.String");
                AddDataColumn(tblPerson, "isDeceased", "System.String");
                AddDataColumn(tblPerson, "gender", "System.String");
                AddDataColumn(tblPerson, "nationality", "System.String");
                AddDataColumn(tblPerson, "imageURL", "System.String");
                AddDataColumn(tblPerson, "telephoneNumber", "System.String");
                AddDataColumn(tblPerson, "faxNumber", "System.String");
                AddDataColumn(tblPerson, "mobileNumber", "System.String");
                AddDataColumn(tblPerson, "email", "System.String");
                AddDataColumn(tblPerson, "pepLevel", "System.Int32");
                AddDataColumn(tblPerson, "isPEP", "System.String");
                AddDataColumn(tblPerson, "isSanctionsCurrent", "System.String");
                AddDataColumn(tblPerson, "isSanctionsPrevious", "System.String");
                AddDataColumn(tblPerson, "isLawEnforcement", "System.String");
                AddDataColumn(tblPerson, "isFinancialregulator", "System.String");
                AddDataColumn(tblPerson, "isDisqualifiedDirector", "System.String");
                AddDataColumn(tblPerson, "isInsolvent", "System.String");
                AddDataColumn(tblPerson, "isAdverseMedia", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblPerson.");
            }
        }

        public void CreateAddressColumns(DataTable tblAddress) 
        {
            try
            {
                AddDataColumn(tblAddress, "id", "System.String");
                AddDataColumn(tblAddress, "address1", "System.String");
                AddDataColumn(tblAddress, "address2", "System.String");
                AddDataColumn(tblAddress, "address3", "System.String");
                AddDataColumn(tblAddress, "address4", "System.String");
                AddDataColumn(tblAddress, "city", "System.String");
                AddDataColumn(tblAddress, "county", "System.String");
                AddDataColumn(tblAddress, "postcode", "System.String");
                AddDataColumn(tblAddress, "country", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblAddress.");
            }
        }

        public void CreateAliasColumns(DataTable tblAlias) 
        {
            try
            {
                AddDataColumn(tblAlias, "id", "System.String");
                AddDataColumn(tblAlias, "title", "System.String");
                AddDataColumn(tblAlias, "alternativeTitle", "System.String");
                AddDataColumn(tblAlias, "forename", "System.String");
                AddDataColumn(tblAlias, "middlename", "System.String");
                AddDataColumn(tblAlias, "surname", "System.String");
                AddDataColumn(tblAlias, "businessName", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblAlias.");
            }
        }

        public void CreateArticlesColumns(DataTable tblArticles) 
        {
            try
            {
                AddDataColumn(tblArticles, "id", "System.String");
                AddDataColumn(tblArticles, "originalURL", "System.String");
                AddDataColumn(tblArticles, "dateCollected", "System.DateTime");
                AddDataColumn(tblArticles, "c6URL", "System.String");
                AddDataColumn(tblArticles, "ArticleCategory1", "System.String");
                AddDataColumn(tblArticles, "ArticleCategory2", "System.String");
                AddDataColumn(tblArticles, "ArticleCategory3", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblArticles.");
            }
        }

        public void CreateSanctionsColumns(DataTable tblSanctions) 
        {
            try
            {
                AddDataColumn(tblSanctions, "id", "System.String");
                AddDataColumn(tblSanctions, "sanctionType", "System.String");
                AddDataColumn(tblSanctions, "isCurrent", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblSanctions.");
            }
        }

        public void CreateNotesColumns(DataTable tblNotes) 
        {
            try
            {
                AddDataColumn(tblNotes, "id", "System.String");
                AddDataColumn(tblNotes, "dataSource", "System.String");
                AddDataColumn(tblNotes, "text", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblNotes.");
            }
        }

        public void CreateLinkedBusinessesColumns(DataTable tblLinkedBusinesses) 
        {
            try
            {
                AddDataColumn(tblLinkedBusinesses, "id", "System.String");
                AddDataColumn(tblLinkedBusinesses, "businessName", "System.String");
                AddDataColumn(tblLinkedBusinesses, "position", "System.String");
                AddDataColumn(tblLinkedBusinesses, "linkDescription", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblLinkedBusinesses.");
            }
        }

        public void CreateLinkedPersonsColumns(DataTable tblLinkedPerson) 
        {
            try
            {
                AddDataColumn(tblLinkedPerson, "id", "System.String");
                AddDataColumn(tblLinkedPerson, "personId", "System.String");
                AddDataColumn(tblLinkedPerson, "name", "System.String");
                AddDataColumn(tblLinkedPerson, "association", "System.String");
                AddDataColumn(tblLinkedPerson, "position", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblLinkedPerson.");
            }
        }

        public void CreatePoliticalPositionsColumns(DataTable tblPoliticalPositions) 
        {
            try
            {
                AddDataColumn(tblPoliticalPositions, "id", "System.String");
                AddDataColumn(tblPoliticalPositions, "description", "System.String");
                AddDataColumn(tblPoliticalPositions, "from", "System.DateTime");
                AddDataColumn(tblPoliticalPositions, "to", "System.DateTime");
                AddDataColumn(tblPoliticalPositions, "country", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblPoliticalPositions.");
            }
        }

        public void CreateBusinessColumns(DataTable tblBusiness) 
        {
            try
            {
                AddDataColumn(tblBusiness, "score", "System.Int32");
                AddDataColumn(tblBusiness, "id", "System.String");
                AddDataColumn(tblBusiness, "businessname", "System.String");
                AddDataColumn(tblBusiness, "telephoneNumber", "System.String");
                AddDataColumn(tblBusiness, "faxNumber", "System.String");
                AddDataColumn(tblBusiness, "website", "System.String");
                AddDataColumn(tblBusiness, "isPEP", "System.String");
                AddDataColumn(tblBusiness, "isSanctionsCurrent", "System.String");
                AddDataColumn(tblBusiness, "isSanctionsPrevious", "System.String");
                AddDataColumn(tblBusiness, "isLawEnforcement", "System.String");
                AddDataColumn(tblBusiness, "isFinancialregulator", "System.String");
                AddDataColumn(tblBusiness, "isDisqualifiedDirector", "System.String");
                AddDataColumn(tblBusiness, "isInsolvent", "System.String");
                AddDataColumn(tblBusiness, "isAdverseMedia", "System.String");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the Data Table tblBusiness.");
            }
        }

        private static void AddDataColumn(DataTable dataTable, string name, string type)
        {
            DataColumn dataColumn = new DataColumn(name)
            {
                DataType = System.Type.GetType(type)
            };
            dataTable.Columns.Add(dataColumn);
        }

        public void PopulatePerson(PersonResponse response, DataTable tblPerson) 
        {
            try
            {
                DataRow personRow;
                personRow = tblPerson.NewRow();
                personRow["score"] = response.score.ToString();
                personRow["id"] = response.person.id.ToString();
                personRow["title"] = response.person.title == null ? string.Empty : response.person.title.description.ToString().Substring(0, response.person.title.description.ToString().Length < 100 ? response.person.title.description.ToString().Length : 100);
                personRow["alternativeTitle"] = string.IsNullOrEmpty(response.person.alternativeTitle) ? string.Empty : response.person.alternativeTitle.ToString().Substring(0, response.person.alternativeTitle.ToString().Length < 100 ? response.person.alternativeTitle.ToString().Length : 100);
                personRow["forename"] = string.IsNullOrEmpty(response.person.forename) ? string.Empty : response.person.forename.ToString().Substring(0, response.person.forename.ToString().Length < 200? response.person.forename.ToString().Length : 200);
                personRow["middlename"] = string.IsNullOrEmpty(response.person.middlename) ? string.Empty : response.person.middlename.ToString().Substring(0, response.person.middlename.ToString().Length < 200 ? response.person.middlename.ToString().Length : 200);
                personRow["surname"] = string.IsNullOrEmpty(response.person.surname) ? string.Empty : response.person.surname.ToString().Substring(0, response.person.surname.ToString().Length < 200 ? response.person.surname.ToString().Length : 200);
                personRow["dateOfBirth"] = response.person.dateOfBirth == null ? (object)DBNull.Value : response.person.dateOfBirth;
                personRow["yearOfBirth"] = response.person.yearOfBirth == null ? (object)DBNull.Value : response.person.yearOfBirth.ToString();
                personRow["dateOfDeath"] = response.person.dateOfDeath == null ?(object) DBNull.Value : response.person.dateOfDeath;
                personRow["yearOfDeath"] = response.person.yearOfDeath == null ? (object)DBNull.Value : response.person.yearOfDeath.ToString();
                personRow["isDeceased"] = response.person.isDeceased == null ? (object)DBNull.Value : response.person.isDeceased.ToString();
                personRow["gender"] = string.IsNullOrEmpty(response.person.gender) ? string.Empty : response.person.gender.ToString();
                personRow["nationality"] = string.IsNullOrEmpty(response.person.nationality.nationality) ? string.Empty : response.person.nationality.nationality.ToString().Substring(0, response.person.nationality.nationality.ToString().Length < 100 ? response.person.nationality.nationality.ToString().Length : 100);
                personRow["imageURL"] = string.IsNullOrEmpty(response.person.imageURL) ? string.Empty : response.person.imageURL.ToString().Substring(0, response.person.imageURL.ToString().Length < 200 ? response.person.imageURL.ToString().Length : 200);
                personRow["telephoneNumber"] = string.IsNullOrEmpty(response.person.telephoneNumber)  ? string.Empty : response.person.telephoneNumber.ToString().Substring(0, response.person.telephoneNumber.ToString().Length < 50 ? response.person.telephoneNumber.ToString().Length : 50);
                personRow["faxNumber"] = string.IsNullOrEmpty(response.person.faxNumber) ? string.Empty : response.person.faxNumber.ToString().Substring(0, response.person.faxNumber.ToString().Length < 50 ? response.person.faxNumber.ToString().Length : 50);
                personRow["mobileNumber"] = string.IsNullOrEmpty(response.person.mobileNumber) ? string.Empty : response.person.mobileNumber.ToString().Substring(0, response.person.mobileNumber.ToString().Length < 50 ? response.person.mobileNumber.ToString().Length :50);
                personRow["email"] = string.IsNullOrEmpty(response.person.email) ? string.Empty : response.person.email.ToString().Substring(0, response.person.email.ToString().Length < 100 ? response.person.email.ToString().Length : 50);
                personRow["pepLevel"] = response.person.pepLevel ==null ? (object)DBNull.Value : response.person.pepLevel;
                personRow["isPEP"] = response.person.isPEP == null ? (object)DBNull.Value : response.person.isPEP.ToString();
                personRow["isSanctionsCurrent"] = response.person.isSanctionsCurrent == null ? (object)DBNull.Value : response.person.isSanctionsCurrent.ToString();
                personRow["isSanctionsPrevious"] = response.person.isSanctionsPrevious == null ? (object)DBNull.Value : response.person.isSanctionsPrevious.ToString();
                personRow["isLawEnforcement"] = response.person.isLawEnforcement == null ? (object)DBNull.Value : response.person.isLawEnforcement.ToString();
                personRow["isFinancialregulator"] = response.person.isFinancialregulator == null ? (object)DBNull.Value : response.person.isFinancialregulator.ToString();
                personRow["isDisqualifiedDirector"] = response.person.isDisqualifiedDirector == null ? (object)DBNull.Value : response.person.isDisqualifiedDirector.ToString();
                personRow["isInsolvent"] = response.person.isInsolvent == null ? (object)DBNull.Value : response.person.isInsolvent.ToString();
                personRow["isAdverseMedia"] = response.person.isAdverseMedia == null ? (object)DBNull.Value : response.person.isAdverseMedia.ToString();
                tblPerson.Rows.Add(personRow);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblPerson.");
            }
        }

        public void PopulateBusiness(BusinessResponse response, DataTable tblPerson) 
        {
            try
            {
                DataRow personRow;
                personRow = tblPerson.NewRow();
                personRow["score"] = response.score.ToString();
                personRow["id"] = response.business.id.ToString();
                personRow["businessname"] = string.IsNullOrEmpty(response.business.businessName) ? string.Empty : response.business.businessName.ToString().Substring(0, response.business.businessName.ToString().Length < 200 ? response.business.businessName.ToString().Length-1 : 200);
                personRow["telephoneNumber"] = string.IsNullOrEmpty(response.business.telephoneNumber) ? string.Empty : response.business.telephoneNumber.ToString().Substring(0, response.business.telephoneNumber.ToString().Length < 50 ? response.business.telephoneNumber.ToString().Length : 50);
                personRow["faxNumber"] = string.IsNullOrEmpty(response.business.faxNumber) ? string.Empty : response.business.faxNumber.ToString();
                personRow["website"] = string.IsNullOrEmpty(response.business.website) ? string.Empty : response.business.website.ToString();
                personRow["isPEP"] = response.business.isPEP == null ? (object)DBNull.Value : response.business.isPEP.ToString();
                personRow["isSanctionsCurrent"] = response.business.isSanctionsCurrent == null ? (object)DBNull.Value : response.business.isSanctionsCurrent.ToString();
                personRow["isSanctionsPrevious"] = response.business.isSanctionsPrevious == null ? (object)DBNull.Value : response.business.isSanctionsPrevious.ToString();
                personRow["isLawEnforcement"] = response.business.isLawEnforcement == null ? (object)DBNull.Value : response.business.isLawEnforcement.ToString();
                personRow["isFinancialregulator"] = response.business.isFinancialregulator == null ? (object)DBNull.Value : response.business.isFinancialregulator.ToString();
                personRow["isDisqualifiedDirector"] = response.business.isDisqualifiedDirector == null ? (object)DBNull.Value : response.business.isDisqualifiedDirector.ToString();
                personRow["isInsolvent"] = response.business.isInsolvent == null ? (object)DBNull.Value : response.business.isInsolvent.ToString();
                personRow["isAdverseMedia"] = response.business.isAdverseMedia == null ? (object)DBNull.Value : response.business.isAdverseMedia.ToString();
                tblPerson.Rows.Add(personRow);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblPerson.");
            }
        }

        public void PopulateAddresses(PersonResponse response, DataTable tblAddresses) 
        {
            try
            {
                for (int i = 0; i < response.person.addresses.Length ; i++)
                {
                    DataRow addressesRow;
                    addressesRow = tblAddresses.NewRow();
                    addressesRow["id"] = response.person.id.ToString();
                    addressesRow["address1"] = string.IsNullOrEmpty(response.person.addresses[i].address1) ? string.Empty : response.person.addresses[i].address1.ToString().Substring(0, response.person.addresses[i].address1.ToString().Length < 200 ? response.person.addresses[i].address1.ToString().Length : 200);
                    addressesRow["address2"] = string.IsNullOrEmpty(response.person.addresses[i].address2) ? string.Empty : response.person.addresses[i].address2.ToString().Substring(0, response.person.addresses[i].address2.ToString().Length < 200 ? response.person.addresses[i].address2.ToString().Length : 200);
                    addressesRow["address3"] = string.IsNullOrEmpty(response.person.addresses[i].address3) ? string.Empty : response.person.addresses[i].address3.ToString().Substring(0, response.person.addresses[i].address3.ToString().Length < 200 ? response.person.addresses[i].address3.ToString().Length : 200);
                    addressesRow["address4"] = string.IsNullOrEmpty(response.person.addresses[i].address4) ? string.Empty : response.person.addresses[i].address4.ToString().Substring(0, response.person.addresses[i].address4.ToString().Length < 200 ? response.person.addresses[i].address4.ToString().Length : 200);
                    addressesRow["city"] = string.IsNullOrEmpty(response.person.addresses[i].city) ? string.Empty : response.person.addresses[i].city.ToString().Substring(0, response.person.addresses[i].city.ToString().Length < 100 ? response.person.addresses[i].city.ToString().Length : 100);
                    addressesRow["county"] = string.IsNullOrEmpty(response.person.addresses[i].county) ? string.Empty : response.person.addresses[i].county.ToString().Substring(0, response.person.addresses[i].county.ToString().Length < 100 ? response.person.addresses[i].county.ToString().Length : 100);
                    addressesRow["postcode"] = string.IsNullOrEmpty(response.person.addresses[i].postcode) ? string.Empty : response.person.addresses[i].postcode.ToString().Substring(0, response.person.addresses[i].postcode.ToString().Length < 10 ? response.person.addresses[i].postcode.ToString().Length : 10);
                    addressesRow["country"] = string.IsNullOrEmpty(response.person.addresses[i].country.name) ? string.Empty : response.person.addresses[i].country.name.ToString().Substring(0, response.person.addresses[i].country.name.ToString().Length < 255 ? response.person.addresses[i].country.name.ToString().Length : 255);
                    tblAddresses.Rows.Add(addressesRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblAddresses.");
            }
        }

        public void PopulateAddresses(BusinessResponse response, DataTable tblAddresses)
        {
            try
            {
                for (int i = 0; i < response.business.addresses.Length; i++)
                {
                    DataRow addressesRow;
                    addressesRow = tblAddresses.NewRow();
                    addressesRow["id"] = response.business.id.ToString();
                    addressesRow["address1"] = string.IsNullOrEmpty(response.business.addresses[i].address1) ? string.Empty : response.business.addresses[i].address1.ToString().Substring(0, response.business.addresses[i].address1.ToString().Length < 200 ? response.business.addresses[i].address1.ToString().Length : 200);
                    addressesRow["address2"] = string.IsNullOrEmpty(response.business.addresses[i].address2) ? string.Empty : response.business.addresses[i].address2.ToString().Substring(0, response.business.addresses[i].address2.ToString().Length < 200 ? response.business.addresses[i].address2.ToString().Length : 200);
                    addressesRow["address3"] = string.IsNullOrEmpty(response.business.addresses[i].address3) ? string.Empty : response.business.addresses[i].address3.ToString().Substring(0, response.business.addresses[i].address3.ToString().Length < 200 ? response.business.addresses[i].address3.ToString().Length : 200);
                    addressesRow["address4"] = string.IsNullOrEmpty(response.business.addresses[i].address4) ? string.Empty : response.business.addresses[i].address4.ToString().Substring(0, response.business.addresses[i].address4.ToString().Length < 200 ? response.business.addresses[i].address4.ToString().Length : 200);
                    addressesRow["city"] = string.IsNullOrEmpty(response.business.addresses[i].city) ? string.Empty : response.business.addresses[i].city.ToString().Substring(0, response.business.addresses[i].city.ToString().Length < 100 ? response.business.addresses[i].city.ToString().Length : 100);
                    addressesRow["county"] = string.IsNullOrEmpty(response.business.addresses[i].county) ? string.Empty : response.business.addresses[i].county.ToString().Substring(0, response.business.addresses[i].county.ToString().Length < 100 ? response.business.addresses[i].county.ToString().Length : 100);
                    addressesRow["postcode"] = string.IsNullOrEmpty(response.business.addresses[i].postcode) ? string.Empty : response.business.addresses[i].postcode.ToString().Substring(0, response.business.addresses[i].postcode.ToString().Length < 10 ? response.business.addresses[i].postcode.ToString().Length :10);
                    addressesRow["country"] = string.IsNullOrEmpty(response.business.addresses[i].country.name) ? string.Empty : response.business.addresses[i].country.name.ToString().Substring(0, response.business.addresses[i].country.name.ToString().Length < 255 ? response.business.addresses[i].country.name.ToString().Length : 255);
                    tblAddresses.Rows.Add(addressesRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblAddresses.");
            }
        }

        public void PopulateAlias(PersonResponse response, DataTable tblAlias) 
        {
            try
            {
                for (int i = 0; i < response.person.aliases.Length; i++)
                {
                    DataRow aliasRow;
                    aliasRow = tblAlias.NewRow();
                    aliasRow["id"] = response.person.id.ToString();
                    aliasRow["title"] = string.IsNullOrEmpty(response.person.aliases[i].title)? string.Empty : response.person.aliases[i].title.ToString().Substring(0, response.person.aliases[i].title.ToString().Length < 100 ? response.person.aliases[i].title.ToString().Length :100);
                    aliasRow["alternativeTitle"] = string.IsNullOrEmpty(response.person.aliases[i].alternativeTitle)? string.Empty : response.person.aliases[i].alternativeTitle.ToString().Substring(0, response.person.aliases[i].alternativeTitle.ToString().Length < 100 ? response.person.aliases[i].alternativeTitle.ToString().Length : 100);
                    aliasRow["forename"] = string.IsNullOrEmpty(response.person.aliases[i].forename) ? string.Empty : response.person.aliases[i].forename.ToString().Substring(0, response.person.aliases[i].forename.ToString().Length < 200 ? response.person.aliases[i].forename.ToString().Length : 200);
                    aliasRow["middlename"] = string.IsNullOrEmpty(response.person.aliases[i].middlename) ? string.Empty : response.person.aliases[i].middlename.ToString().Substring(0, response.person.aliases[i].middlename.ToString().Length < 200 ? response.person.aliases[i].middlename.ToString().Length : 200);
                    aliasRow["surname"] = string.IsNullOrEmpty(response.person.aliases[i].surname) ? string.Empty : response.person.aliases[i].surname.ToString().Substring(0, response.person.aliases[i].surname.ToString().Length < 200 ? response.person.aliases[i].surname.ToString().Length : 200);
                    aliasRow["businessName"] = string.IsNullOrEmpty(response.person.aliases[i].businessName) ? string.Empty : response.person.aliases[i].businessName.ToString().Substring(0, response.person.aliases[i].businessName.ToString().Length < 200 ? response.person.aliases[i].businessName.ToString().Length : 200);
                    tblAlias.Rows.Add(aliasRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblAlias.");
            }
        }

        public void PopulateAlias(BusinessResponse response, DataTable tblAlias)
        {
            try
            {
                for (int i = 0; i < response.business.aliases.Length; i++)
                {
                    DataRow aliasRow;
                    aliasRow = tblAlias.NewRow();
                    aliasRow["id"] = response.business.id.ToString();
                    aliasRow["title"] = string.IsNullOrEmpty(response.business.aliases[i].title) ? string.Empty : response.business.aliases[i].title.ToString().Substring(0, response.business.aliases[i].title.ToString().Length < 100 ? response.business.aliases[i].title.ToString().Length : 100);
                    aliasRow["alternativeTitle"] = string.IsNullOrEmpty(response.business.aliases[i].alternativeTitle) ? string.Empty : response.business.aliases[i].alternativeTitle.ToString().Substring(0, response.business.aliases[i].alternativeTitle.ToString().Length < 100 ? response.business.aliases[i].alternativeTitle.ToString().Length : 100);
                    aliasRow["forename"] = string.IsNullOrEmpty(response.business.aliases[i].forename) ? string.Empty : response.business.aliases[i].forename.ToString().Substring(0, response.business.aliases[i].forename.ToString().Length < 200 ? response.business.aliases[i].forename.ToString().Length : 200);
                    aliasRow["middlename"] = string.IsNullOrEmpty(response.business.aliases[i].middlename) ? string.Empty : response.business.aliases[i].middlename.ToString().Substring(0, response.business.aliases[i].middlename.ToString().Length < 200 ? response.business.aliases[i].middlename.ToString().Length : 200);
                    aliasRow["surname"] = string.IsNullOrEmpty(response.business.aliases[i].surname) ? string.Empty : response.business.aliases[i].surname.ToString().Substring(0, response.business.aliases[i].surname.ToString().Length < 200 ? response.business.aliases[i].surname.ToString().Length : 200);
                    aliasRow["businessName"] = string.IsNullOrEmpty(response.business.aliases[i].businessName) ? string.Empty : response.business.aliases[i].businessName.ToString().Substring(0, response.business.aliases[i].businessName.ToString().Length < 200 ? response.business.aliases[i].businessName.ToString().Length : 200);
                    tblAlias.Rows.Add(aliasRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblAlias.");
            }
        }

        public void PopulateArticles(PersonResponse response, DataTable tblArticles)
        {
            try
            {
                for (int i = 0; i < response.person.articles.Length; i++)
                {
                    DataRow articleRow;
                    articleRow = tblArticles.NewRow();
                    articleRow["id"] = response.person.id.ToString();
                    articleRow["originalURL"] = string.IsNullOrEmpty(response.person.articles[i].originalURL)? string.Empty : response.person.articles[i].originalURL.ToString().Substring(0, response.person.articles[i].originalURL.ToString().Length < 1000 ? response.person.articles[i].originalURL.ToString().Length : 1000);
                    articleRow["dateCollected"] = string.IsNullOrEmpty(response.person.articles[i].dateCollected)? string.Empty : response.person.articles[i].dateCollected.ToString();
                    articleRow["c6URL"] = string.IsNullOrEmpty(response.person.articles[i].c6URL)? string.Empty : response.person.articles[i].c6URL.ToString().Substring(0, response.person.articles[i].c6URL.ToString().Length < 1000 ? response.person.articles[i].c6URL.ToString().Length : 1000);
                    if (response.person.articles[i].categories.Length > 0)
                    {
                        for (int c = 0; c < (response.person.articles[i].categories.Length > 3 ? 3 : response.person.articles[i].categories.Length); c++)
                        {
                            articleRow[$"ArticleCategory{c+1}"] = string.IsNullOrEmpty(response.person.articles[i].categories[c].name)? string.Empty : response.person.articles[i].categories[c].name.ToString().Substring(0, response.person.articles[i].categories[c].name.ToString().Length < 50 ? response.person.articles[i].categories[c].name.ToString().Length : 50);
                        }
                    }
                    tblArticles.Rows.Add(articleRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblArticles.");
            }
        }

        public void PopulateArticles(BusinessResponse response, DataTable tblArticles)
        {
            try
            {
                for (int i = 0; i < response.business.articles.Length; i++)
                {
                    DataRow articleRow;
                    articleRow = tblArticles.NewRow();
                    articleRow["id"] = response.business.id.ToString();
                    articleRow["originalURL"] = string.IsNullOrEmpty(response.business.articles[i].originalURL) ? string.Empty : response.business.articles[i].originalURL.ToString().Substring(0, response.business.articles[i].originalURL.ToString().Length < 200 ? response.business.articles[i].originalURL.ToString().Length : 200);
                    articleRow["dateCollected"] = response.business.articles[i].dateCollected == null ? (object)DBNull.Value : response.business.articles[i].dateCollected;
                    articleRow["c6URL"] = string.IsNullOrEmpty(response.business.articles[i].c6URL) ? string.Empty : response.business.articles[i].c6URL.ToString().Substring(0, response.business.articles[i].c6URL.ToString().Length < 1000 ? response.business.articles[i].c6URL.ToString().Length : 1000);
                    if (response.business.articles[i].categories.Length > 0)
                    {
                        for (int c = 0; c < (response.business.articles[i].categories.Length > 3 ? 3: response.business.articles[i].categories.Length); c++)
                        {
                            articleRow[$"ArticleCategory{c + 1}"] = string.IsNullOrEmpty(response.business.articles[i].categories[c].name) ? string.Empty : response.business.articles[i].categories[c].name.ToString().Substring(0, response.business.articles[i].categories[c].name.ToString().Length < 50 ? response.business.articles[i].categories[c].name.ToString().Length  : 50);
                        }
                    }
                    tblArticles.Rows.Add(articleRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblArticles.");
            }
        }

        public void PopulateSanctions(PersonResponse response, DataTable tblSanctions) 
        {
            try
            {
                for (int i = 0; i < response.person.sanctions.Length; i++)
                {
                    DataRow sanctionsRow;
                    sanctionsRow = tblSanctions.NewRow();
                    sanctionsRow["id"] = response.person.id.ToString();
                    sanctionsRow["sanctionType"] = response.person.sanctions[i].sanctionType == null ? (object)DBNull.Value : response.person.sanctions[i].sanctionType.description.ToString().Substring(0, response.person.sanctions[i].sanctionType.description.ToString().Length < 50 ? response.person.sanctions[i].sanctionType.description.ToString().Length : 50);
                    sanctionsRow["isCurrent"] = response.person.sanctions[i].isCurrent == null ? (object)DBNull.Value : response.person.sanctions[i].isCurrent.ToString();
                    tblSanctions.Rows.Add(sanctionsRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblSanctions.");
            }
        }

        public void PopulateSanctions(BusinessResponse response, DataTable tblSanctions)
        {
            try
            {
                for (int i = 0; i < response.business.sanctions.Length; i++)
                {
                    DataRow sanctionsRow;
                    sanctionsRow = tblSanctions.NewRow();
                    sanctionsRow["id"] = response.business.id.ToString();
                    sanctionsRow["sanctionType"] = response.business.sanctions[i].sanctionType.description == null ? (object)DBNull.Value : response.business.sanctions[i].sanctionType.description.ToString().Substring(0, response.business.sanctions[i].sanctionType.ToString().Length < 50 ? response.business.sanctions[i].sanctionType.ToString().Length : 50);
                    sanctionsRow["isCurrent"] = string.IsNullOrEmpty(response.business.sanctions[i].isCurrent)? string.Empty : response.business.sanctions[i].isCurrent.ToString();
                    tblSanctions.Rows.Add(sanctionsRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblSanctions.");
            }
        }

        public void PopulateNotes(PersonResponse response, DataTable tblNotes) 
        {
            try
            {
                for (int i = 0; i < response.person.notes.Length; i++)
                {
                    DataRow notesRow;
                    notesRow = tblNotes.NewRow();
                    notesRow["id"] = response.person.id.ToString();
                    notesRow["dataSource"] = response.person.notes[i].dataSource==null ? string.Empty : response.person.notes[i].dataSource.name.ToString().Substring(0, response.person.notes[i].dataSource.name.ToString().Length < 100 ? response.person.notes[i].dataSource.name.ToString().Length : 100);
                    notesRow["text"] = string.IsNullOrEmpty(response.person.notes[i].text) ? string.Empty : response.person.notes[i].text.ToString();
                    tblNotes.Rows.Add(notesRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblNotes.");
            }
        }

        public void PopulateNotes(BusinessResponse response, DataTable tblNotes)
        {
            try
            {
                for (int i = 0; i < response.business.notes.Length; i++)
                {
                    DataRow notesRow;
                    notesRow = tblNotes.NewRow();
                    notesRow["id"] = response.business.id.ToString();
                    notesRow["dataSource"] = response.business.notes[i].dataSource == null ? string.Empty : response.business.notes[i].dataSource.name.ToString().Substring(0, response.business.notes[i].dataSource.name.ToString().Length < 100 ? response.business.notes[i].dataSource.name.ToString().Length : 100);
                    notesRow["text"] = string.IsNullOrEmpty(response.business.notes[i].text) ? string.Empty : response.business.notes[i].text.ToString();
                    tblNotes.Rows.Add(notesRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblNotes.");
            }
        }

        public void PopulateLinkedBusinesses(PersonResponse response, DataTable tblLinkedBusinesses) 
        {
            try
            {
                for (int i = 0; i < response.person.linkedBusinesses.Length; i++)
                {
                    DataRow linkedBusinessesRow;
                    linkedBusinessesRow = tblLinkedBusinesses.NewRow();
                    linkedBusinessesRow["id"] = response.person.id.ToString();
                    linkedBusinessesRow["businessName"] = string.IsNullOrEmpty(response.person.linkedBusinesses[i].businessName) ? string.Empty : response.person.linkedBusinesses[i].businessName.ToString().Substring(0, response.person.linkedBusinesses[i].businessName.ToString().Length  < 200 ? response.person.linkedBusinesses[i].businessName.ToString().Length : 200);
                    linkedBusinessesRow["linkDescription"] = string.Empty;
                    linkedBusinessesRow["position"] = string.IsNullOrEmpty(response.person.linkedBusinesses[i].position) ? string.Empty : response.person.linkedBusinesses[i].position.ToString().Substring(0, response.person.linkedBusinesses[i].position.ToString().Length < 200 ? response.person.linkedBusinesses[i].position.ToString().Length : 200);
                    tblLinkedBusinesses.Rows.Add(linkedBusinessesRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblLinkedBusinesses.");
            }
        }

        public void PopulateLinkedBusinesses(BusinessResponse response, DataTable tblLinkedBusinesses)
        {
            try
            {
                for (int i = 0; i < response.business.linkedBusinesses.Length; i++)
                {
                    DataRow linkedBusinessesRow;
                    linkedBusinessesRow = tblLinkedBusinesses.NewRow();
                    linkedBusinessesRow["id"] = response.business.id.ToString();
                    linkedBusinessesRow["businessName"] = string.IsNullOrEmpty(response.business.linkedBusinesses[i].businessName) ? string.Empty : response.business.linkedBusinesses[i].businessName.ToString().Substring(0, response.business.linkedBusinesses[i].businessName.ToString().Length < 200 ? response.business.linkedBusinesses[i].businessName.ToString().Length : 200);
                    linkedBusinessesRow["linkDescription"] = string.IsNullOrEmpty(response.business.linkedBusinesses[i].linkDescription) ? string.Empty : response.business.linkedBusinesses[i].linkDescription.ToString().Substring(0, response.business.linkedBusinesses[i].linkDescription.ToString().Length < 200 ? response.business.linkedBusinesses[i].linkDescription.ToString().Length : 200);
                    linkedBusinessesRow["position"] = string.Empty;
                    tblLinkedBusinesses.Rows.Add(linkedBusinessesRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblLinkedBusinesses.");
            }
        }

        public void PopulateLinkedPersons(PersonResponse response, DataTable tblLinkedPersons) 
        {
            try
            {
                for (int i = 0; i < response.person.linkedPersons.Length; i++)
                {
                    DataRow linkedPersonsRow;
                    linkedPersonsRow = tblLinkedPersons.NewRow();
                    linkedPersonsRow["id"] = response.person.id.ToString();
                    linkedPersonsRow["personId"] = response.person.linkedPersons[i].personId == null ? (object)DBNull.Value : response.person.linkedPersons[i].personId.ToString();
                    linkedPersonsRow["name"] = string.IsNullOrEmpty(response.person.linkedPersons[i].name) ? string.Empty : response.person.linkedPersons[i].name.ToString().Substring(0, response.person.linkedPersons[i].name.ToString().Length < 200 ? response.person.linkedPersons[i].name.ToString().Length : 200);
                    linkedPersonsRow["association"] = string.IsNullOrEmpty(response.person.linkedPersons[i].association) ? string.Empty : response.person.linkedPersons[i].association.ToString().Substring(0, response.person.linkedPersons[i].association.ToString().Length < 200 ? response.person.linkedPersons[i].association.ToString().Length : 200);
                    linkedPersonsRow["position"] = string.Empty;
                    tblLinkedPersons.Rows.Add(linkedPersonsRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblLinkedPersons.");
            }
        }

        public void PopulateLinkedPersons(BusinessResponse response, DataTable tblLinkedPersons)
        {
            try
            {
                for (int i = 0; i < response.business.linkedPersons.Length; i++)
                {
                    DataRow linkedPersonsRow;
                    linkedPersonsRow = tblLinkedPersons.NewRow();
                    linkedPersonsRow["id"] = response.business.id.ToString();
                    linkedPersonsRow["personId"] = response.business.linkedPersons[i].personId == null ? (object)DBNull.Value : response.business.linkedPersons[i].personId.ToString();
                    linkedPersonsRow["name"] = string.IsNullOrEmpty(response.business.linkedPersons[i].name) ? string.Empty : response.business.linkedPersons[i].name.ToString().Substring(0, response.business.linkedPersons[i].name.ToString().Length < 200 ? response.business.linkedPersons[i].name.ToString().Length : 200);
                    linkedPersonsRow["association"] = string.Empty;
                    linkedPersonsRow["position"] = string.IsNullOrEmpty(response.business.linkedPersons[i].position) ? string.Empty : response.business.linkedPersons[i].position.ToString().Substring(0, response.business.linkedPersons[i].position.ToString().Length < 200 ? response.business.linkedPersons[i].position.ToString().Length : 200);
                    tblLinkedPersons.Rows.Add(linkedPersonsRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblLinkedPersons.");
            }
        }

        public void PopulatePoliticalPositions(PersonResponse response, DataTable tblPoliticalPositions) 
        {
            try
            {
                for (int i = 0; i < response.person.politicalPositions.Length; i++)
                {
                    DataRow politicalPositionsRow;
                    politicalPositionsRow = tblPoliticalPositions.NewRow();
                    politicalPositionsRow["id"] = response.person.id.ToString();
                    politicalPositionsRow["description"] = string.IsNullOrEmpty(response.person.politicalPositions[i].description) ? string.Empty : response.person.politicalPositions[i].description.ToString().Substring(0, response.person.politicalPositions[i].description.ToString().Length < 200 ? response.person.politicalPositions[i].description.ToString().Length : 200);
                    if (DateTime.TryParse(response.person.politicalPositions[i].from, out DateTime tempFrom)==true)
                    {
                        politicalPositionsRow["from"] = response.person.politicalPositions[i].from;
                    }
                    else
                    {
                        politicalPositionsRow["from"] = string.IsNullOrEmpty(response.person.politicalPositions[i].from) ? (object)DBNull.Value : DateTime.Parse($"01-Jan-{response.person.politicalPositions[i].from}", CultureInfo.InvariantCulture);
                    }
                    if (DateTime.TryParse(response.person.politicalPositions[i].to, out DateTime tempTo) == true)
                    {
                        politicalPositionsRow["to"] = response.person.politicalPositions[i].to;
                    }
                    else
                    {
                        politicalPositionsRow["from"] = string.IsNullOrEmpty(response.person.politicalPositions[i].to) ? (object)DBNull.Value : DateTime.Parse($"01-Jan-{response.person.politicalPositions[i].to}", CultureInfo.InvariantCulture); ;
                    }
                    politicalPositionsRow["country"] = string.IsNullOrEmpty(response.person.politicalPositions[i].country.name) ? string.Empty : response.person.politicalPositions[i].country.name.ToString().Substring(0, response.person.politicalPositions[i].country.name.ToString().Length < 200 ? response.person.politicalPositions[i].country.name.ToString().Length : 200);
                    tblPoliticalPositions.Rows.Add(politicalPositionsRow);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error populating the table tblPoliticalPositions.");
            }
        }
    }
}
