﻿using CreditSafeApi.Models.Api;
using CreditSafeApi.Models.Configuration;
using CreditSafeApi.Repositories.Interfaces;
using CreditSafeApi.Repositories.Repo_Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

using DataType = System.Data;

namespace CreditSafeApi.Repositories
{
    public class SqlRepository : ISql
    {
        private readonly CreditSafeConfig _creditSafeConfig;
        private readonly ILogger _logger;

        public SqlRepository(CreditSafeConfig creditSafeConfig, ILogger logger)
        {
            _creditSafeConfig = creditSafeConfig;
            _logger = logger;
        }

        /// <summary>
        /// Retrieve the client details by clientId.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<ClientDetails> FetchClientDetails(double clientId)
        {
            ClientDetails clientDetails = new ClientDetails();
            try
            {
                using var conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using var comm = new SqlCommand(_creditSafeConfig.FetchClientDetailsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    clientDetails.ClientId = clientId.ToString();
                    clientDetails.Name = reader["Name"].ToString();
                    clientDetails.CompanyName = reader["Company Name"].ToString();
                    clientDetails.Website = reader["Website"].ToString();
                    clientDetails.Email = reader["Email"].ToString();
                    clientDetails.Password = reader["Password"].ToString();
                    clientDetails.ApiKey = reader["ApiKey"].ToString();
                    clientDetails.SearchRequestsAvailable = (int)reader["SearchRequestsAvailable"];
                    clientDetails.Active = (bool)reader["Active"];
                    clientDetails.SearchRequestsAllocated = (int)reader["SearchRequestsAllocated"];
                    clientDetails.SearchRequestsUsed = (int)reader["SearchRequestsUsed"];
                    clientDetails.SwiftRequestsAvailable = (int)reader["SwiftRequestsAvailable"];
                    clientDetails.SwiftRequestsAllocated = (int)reader["SwiftRequestsAllocated"];
                    clientDetails.SwiftRequestsUsed = (int)reader["SwiftRequestsUsed"];
                    clientDetails.EndDate = (DateTime)reader["EndDate"];
                }
                return clientDetails;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error fetching client details. ClientId={ClientId}", clientId);
                return null;
            }
        }

        /// <summary>
        /// Retrieve the client details by email.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<ClientDetails> FetchClientDetails(ResendVerificationRequest resendVerificationRequest)
        {
            ClientDetails clientDetails = new ClientDetails();
            try
            {
                using var conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using var comm = new SqlCommand(_creditSafeConfig.FetchClientDetailsByEmailStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@Email", SqlDbType.VarChar, 150).Value = resendVerificationRequest.Email;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    clientDetails.ClientId = reader["ClientId"].ToString();
                    clientDetails.Name = reader["Name"].ToString();
                    clientDetails.CompanyName = reader["Company Name"].ToString();
                    clientDetails.Website = reader["Website"].ToString();
                    clientDetails.Email = reader["Email"].ToString();
                    clientDetails.Password = reader["Password"].ToString();
                    clientDetails.ApiKey = reader["ApiKey"].ToString();
                    clientDetails.SwiftRequestsAvailable = (int)reader["SwiftRequestsAvailable"];
                    clientDetails.Active = (bool)reader["Active"];
                    clientDetails.SearchRequestsAllocated = (int)reader["SearchRequestsAllocated"];
                    clientDetails.SearchRequestsUsed = (int)reader["SearchRequestsUsed"];
                    clientDetails.SearchRequestsAvailable = (int)reader["SearchRequestsAvailable"];
                    clientDetails.SwiftRequestsAllocated = (int)reader["SwiftRequestsAllocated"];
                    clientDetails.SwiftRequestsUsed = (int)reader["SwiftRequestsUsed"];
                    clientDetails.EndDate = (DateTime)reader["EndDate"];
                    clientDetails.AuthDate = reader["AuthDate"] == DBNull.Value ? (DateTime?)null : (DateTime)reader["AuthDate"];
                    clientDetails.Payments = (bool)reader["Payments"];
                    clientDetails.FOP = (bool)reader["FOP"];
                    clientDetails.Orders = (bool)reader["Orders"];
                    clientDetails.Settlement = (bool)reader["Settlement"];
                    clientDetails.CorporateActions = (bool)reader["CorporateActions"];
                    clientDetails.Swift = (bool)reader["Swift"];
                    clientDetails.SystemReconciliation = (bool)reader["SystemReconciliation"];
                }
                _logger.LogInformation("Client details located for the email address {}.", resendVerificationRequest.Email);
                return clientDetails;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error fetching client details by email ({}).", resendVerificationRequest.Email);
                return null;
            }
        }

        /// <summary>
        /// Call the stored procedure DolfinPaymentApiUpdateClient to update the client details.
        /// </summary>
        /// <param name="clientUpdateRequest"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<bool> UpdateClientDetails(ClientUpdateRequest clientUpdateRequest, double clientId)
        {
            try
            {
                using var conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using var comm = new SqlCommand(_creditSafeConfig.UpdateClientDetailsStoredProcedure, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@Title", SqlDbType.VarChar, 15).Value = TrimValue(clientUpdateRequest.Title);
                comm.Parameters.Add("@Forename", SqlDbType.VarChar, 100).Value = TrimValue(clientUpdateRequest.Forename);
                comm.Parameters.Add("@Middlename", SqlDbType.VarChar, 100).Value = TrimValue(clientUpdateRequest.Middlename);
                comm.Parameters.Add("@Surname", SqlDbType.VarChar, 100).Value = TrimValue(clientUpdateRequest.Surname);
                comm.Parameters.Add("@CompanyName", SqlDbType.VarChar, 100).Value = TrimValue(clientUpdateRequest.CompanyName);
                comm.Parameters.Add("@Website", SqlDbType.VarChar, 150).Value = clientUpdateRequest.Website;
                comm.Parameters.Add("@Email", SqlDbType.VarChar, 150).Value = TrimValue(clientUpdateRequest.Email);
                comm.Parameters.Add("@Password", SqlDbType.VarChar, 150).Value = TrimValue(clientUpdateRequest.Password);
                comm.Parameters.Add("@UpdatedBy", SqlDbType.VarChar).Value = TrimValue(clientUpdateRequest.updatedBy);
                await comm.ExecuteNonQueryAsync();
                _logger.LogInformation("Sucessfully updated the new details | Payload {}. ClientId={ClientId}", JSON.Serializer(clientUpdateRequest), clientId);

                return true;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure DolfinPaymentApiUpdateClient. ClientId={ClientId}", clientId);
                return false;
            }
        }

        /// <summary>
        /// Fetch the client details by client id.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="hidePassord"></param>
        /// <returns></returns>
        public async Task<ClientDetails> FetchClient(double clientId, bool hidePassord = false)
        {
            ClientDetails clientDetails = new ClientDetails();
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.FetchClientDetailsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    clientDetails.ClientId = clientId.ToString();
                    clientDetails.Title = reader["Title"].ToString();
                    clientDetails.Name = reader["Name"].ToString();
                    clientDetails.CompanyName = reader["Company Name"].ToString();
                    clientDetails.Website = reader["Website"].ToString();
                    clientDetails.Email = reader["Email"].ToString();
                    clientDetails.Password = hidePassord == true ? null : reader["Password"].ToString();
                    clientDetails.ApiKey = reader["ApiKey"].ToString();
                    clientDetails.Active = (bool)reader["Active"];
                    clientDetails.EndDate = (DateTime)reader["EndDate"];
                    clientDetails.SearchRequestsAvailable = int.Parse(reader["SearchRequestsAvailable"].ToString());
                    clientDetails.SearchRequestsAllocated = int.Parse(reader["SearchRequestsAllocated"].ToString());
                    clientDetails.SearchRequestsUsed = int.Parse(reader["SearchRequestsUsed"].ToString());
                    clientDetails.SwiftRequestsAvailable = int.Parse(reader["SwiftRequestsAvailable"].ToString());
                    clientDetails.SwiftRequestsAllocated = int.Parse(reader["SwiftRequestsAllocated"].ToString());
                    clientDetails.SwiftRequestsUsed = int.Parse(reader["SwiftRequestsUsed"].ToString());
                    clientDetails.Payments = (bool)reader["Payments"];
                    clientDetails.FOP = (bool)reader["FOP"];
                    clientDetails.Orders = (bool)reader["Orders"];
                    clientDetails.Settlement = (bool)reader["Settlement"];
                    clientDetails.CorporateActions = (bool)reader["CorporateActions"];
                    clientDetails.Swift = (bool)reader["Swift"];
                    clientDetails.SystemReconciliation = (bool)reader["SystemReconciliation"];
                }
                return clientDetails;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error fetching client. ClientId={ClientId}", clientId);
                return null;
            }
        }

        public async Task<Client> ValidateClient(string email)
        {
            Client client = new Client();
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.FetchClientValidateStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@Email", SqlDbType.VarChar, 150).Value = email;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    client.ClientId = int.Parse(reader["ClientId"].ToString());
                    client.Title = reader["Title"].ToString();
                    client.Forename = reader["Forename"].ToString();
                    client.Middlename = reader["Middlename"].ToString();
                    client.Surname = reader["Surname"].ToString();
                    client.CompanyName = reader["Company Name"].ToString();
                    client.Website = reader["Website"].ToString();
                    client.Email = reader["Email"].ToString();
                    client.Password = reader["Password"].ToString();
                    client.ApiKey = reader["ApiKey"].ToString();
                    client.Active = (bool)reader["Active"];
                    client.EndDate = (DateTime?)reader["EndDate"];
                    client.SearchRequestsAvailable = int.Parse(reader["SearchRequestsAvailable"].ToString());
                    client.SearchRequestsAllocated = int.Parse(reader["SearchRequestsAllocated"].ToString());
                    client.SearchRequestsUsed = int.Parse(reader["SearchRequestsUsed"].ToString());
                    client.SwiftRequestsAvailable = int.Parse(reader["SwiftRequestsAvailable"].ToString());
                    client.SwiftRequestsAllocated = int.Parse(reader["SwiftRequestsAllocated"].ToString());
                    client.SwiftRequestsUsed = int.Parse(reader["SwiftRequestsUsed"].ToString());
                }
                return client;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error validating client, email:{email}.  Error message : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Validate the Client Id value.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<bool> ValidClientId(string clientId)
        {
            ClientDetails clientDetails = new ClientDetails();
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.FetchClientDetailsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    return true;
                }
                return false;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching list of valid countries.");
                return false;
            }
        }

        /// <summary>
        /// Fetch the file paths.
        /// </summary>
        /// <returns></returns>
        public async Task<List<FilePaths>> FetchFilePaths()
        {
            List<FilePaths> filePaths = new List<FilePaths>();
            Extensions extensions = new Extensions();

            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.FetchFilePathsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    filePaths = await extensions.DataReaderMapToList<FilePaths>(reader);
                }
                return filePaths;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching list of valid file paths.");
                return null;
            }
        }

        /// <summary>
        /// Fetch a list of countries to validate against.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Countries>> FetchCountries()
        {
            List<Countries> currencies = new List<Countries>();
            Extensions extensions = new Extensions();

            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.FetchCountriesStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    currencies = await extensions.DataReaderMapToList<Countries>(reader);
                }
                return currencies;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching list of valid countries.");
                return null;
            }
        }

        /// <summary>
        /// Fetch the Api Key for the client Id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<string> FetchApiKey(string userId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.FetchApiKeyStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@ClientId", SqlDbType.Int, 50).Value = userId;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("The ApiKey {} was retrieved successfully. ClientId={ClientId}", result, userId);

                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error fetching the API Key. ClientId={ClientId}", userId);
                return null;
            }
        }

        public async Task<string> FetchAdminId()
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.FetchAdminIdStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("The Admin Id ({}) was retrieved successfully. ClientId={ClientId}", result, result);

                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error fetching the API Key.");
                return null;
            }
        }

        /// <summary>
        /// Insert details into the Client Request Tables, this logs each request made by the client.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="apiKey"></param>
        /// <param name="requestType"></param>
        /// <param name="requestStatus"></param>
        /// <returns></returns>
        public async Task<bool> InsertClientRequest(string clientId, string apiKey, string requestType, string requestStatus)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.InsertClientRequestsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ApiKey", SqlDbType.VarChar, 150).Value = apiKey;
                comm.Parameters.Add("@RequestType", SqlDbType.VarChar, 10).Value = TrimValue(requestType);
                comm.Parameters.Add("@RequestStatus", SqlDbType.VarChar, 50).Value = TrimValue(requestStatus);
                await comm.ExecuteNonQueryAsync();

                _logger.LogInformation("Executed the stored procedure {} sucessfully. ClientId={ClientId}", _creditSafeConfig.InsertClientRequestsStoredProcedure, clientId);

                return true;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}. ClientId={ClientId}", _creditSafeConfig.InsertClientRequestsStoredProcedure, clientId);
                return false;
            }
        }

        public async Task<bool> InsertClientAuthDetails(string clientId, string authKey)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.InsertClientAuthStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@AuthKey", SqlDbType.VarChar, 100).Value = TrimValue(authKey);
                await comm.ExecuteNonQueryAsync();

                _logger.LogInformation("Executed the stored procedure {} sucessfully. ClientId={ClientId}", _creditSafeConfig.InsertClientAuthStoredProcedure, clientId);

                return true;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}. ClientId={ClientId}", _creditSafeConfig.InsertClientAuthStoredProcedure, clientId);
                return false;
            }
        }

        public async Task<ClientResponse> ValidateClientAuthKey(string authKey)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.ValidateClientAuthStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@AuthKey", SqlDbType.VarChar, 100).Value = authKey;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Direction = ParameterDirection.Output;
                comm.Parameters.Add("@Validated", SqlDbType.Bit).Direction = ParameterDirection.Output;
                await comm.ExecuteNonQueryAsync();

                ClientResponse clientValidate = new ClientResponse()
                {
                    ClientId = string.IsNullOrEmpty(comm.Parameters["@ClientId"].Value.ToString()) ? "" : comm.Parameters["@ClientId"].Value.ToString()
                };

                if (!string.IsNullOrEmpty(comm.Parameters["@ClientId"].Value.ToString()))
                {
                    clientValidate.Validated = (bool)comm.Parameters["@Validated"].Value;
                }
                else
                {
                    clientValidate.Validated = false;
                }

                _logger.LogInformation("Executed the stored procedure {} sucessfully. ClientId={ClientId}", _creditSafeConfig.ValidateClientAuthStoredProcedure, comm.Parameters["@ClientId"].Value == null ? "" : comm.Parameters["@ClientId"].Value.ToString());

                return clientValidate;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}.", _creditSafeConfig.ValidateClientAuthStoredProcedure);
                return null;
            }
        }

        /// <summary>
        /// Update the table DolfinPaymentApiClientAuthenticate when a client activates the authentication link in the email sent.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="authKey"></param>
        /// <returns></returns>
        public async Task<bool> UpdateClientAuthKey(string clientId, string authKey)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.UpdateClientAuthStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@AuthKey", SqlDbType.VarChar, 100).Value = authKey;
                await comm.ExecuteNonQueryAsync();

                _logger.LogInformation("Executed the stored procedure {} sucessfully. ClientId={ClientId}", _creditSafeConfig.UpdateClientAuthStoredProcedure, clientId);

                return true;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}. clientId", _creditSafeConfig.UpdateClientAuthStoredProcedure, clientId);
                return false;
            }
        }

        /// <summary>
        /// Inserts new client details into the table DolfinPaymentApiClientDetails.
        /// </summary>
        /// <param name="clientRequest"></param>
        /// <returns></returns>
        public async Task<ClientResponse> InsertClientDetails(ClientRequest clientRequest)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.InsertClientDetailsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@Title", SqlDbType.VarChar, 15).Value = TrimValue(clientRequest.Title);
                comm.Parameters.Add("@Forename", SqlDbType.VarChar, 100).Value = TrimValue(clientRequest.Forename);
                comm.Parameters.Add("@Middlename", SqlDbType.VarChar, 100).Value = TrimValue(clientRequest.Middlename);
                comm.Parameters.Add("@Surmane", SqlDbType.VarChar, 100).Value = TrimValue(clientRequest.Surname);
                comm.Parameters.Add("@CompanyName", SqlDbType.VarChar, 100).Value = TrimValue(clientRequest.CompanyName);
                comm.Parameters.Add("@Website", SqlDbType.VarChar, 150).Value = clientRequest.Website;
                comm.Parameters.Add("@Email", SqlDbType.VarChar, 150).Value = TrimValue(clientRequest.Email);
                comm.Parameters.Add("@Password", SqlDbType.VarChar, 150).Value = TrimValue(clientRequest.Password);
                comm.Parameters.Add("@DailySearchRequestAmt", SqlDbType.Int).Value = clientRequest.SearchAllocation;
                comm.Parameters.Add("@DailySwiftRequestAmt", SqlDbType.Int).Value = clientRequest.SwiftAllocation;
                //comm.Parameters.Add("@EndDate", SqlDbType.Date).Value =  clientRequest.EndDate;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Direction = ParameterDirection.Output;
                comm.Parameters.Add("@ApiKey", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                await comm.ExecuteNonQueryAsync();

                ClientResponse clientInsert = new ClientResponse()
                {
                    ClientId = comm.Parameters["@ClientId"].Value.ToString(),
                    ApiKey = comm.Parameters["@ApiKey"].Value.ToString(),
                    Validated = false
                };

                if (!(string.IsNullOrEmpty(clientInsert.ClientId) || string.IsNullOrEmpty(clientInsert.ApiKey)))
                {
                    _logger.LogInformation("Sucessfully inserted the new client details | ApiKey {}, Payload {}. ClientId={ClientId}", clientInsert.ApiKey, JSON.Serializer(clientRequest), clientInsert.ClientId);
                }
                else
                {
                    _logger.LogError("Failed to insert the new client details for ({}), check audit log for details. | Payload {}.", string.Concat(clientRequest.Forename, " ", clientRequest.Surname), JSON.Serializer(clientRequest));
                }

                return clientInsert;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}.", _creditSafeConfig.InsertClientRequestsStoredProcedure);
                return null;
            }
        }

        /// <summary>
        /// Insert the file details into the table SafeSearchService_FtpFiles.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="ftpFilePath"></param>
        public async Task<bool> InsertFtpFiles(double clientId, string ftpFilePath)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.InsertFtpFilesStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@FtpFile", SqlDbType.VarChar, 150).Value = ftpFilePath;
                await comm.ExecuteNonQueryAsync();
                return true;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}.", _creditSafeConfig.InsertFtpFilesStoredProcedure);
                return false;
            }
        }

        /// <summary>
        /// Check for expire authentication link.
        /// </summary>
        /// <param name="authKey"></param>
        /// <returns></returns>
        public async Task<bool> CheckForExpiredLink(string authKey)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_creditSafeConfig.VerificationLinkExpiredStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@AuthKey", SqlDbType.VarChar, 100).Value = authKey;
                comm.Parameters.Add("@Expired", SqlDbType.Bit).Direction = ParameterDirection.Output;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Direction = ParameterDirection.Output;
                await comm.ExecuteNonQueryAsync();

                bool expired = (bool)comm.Parameters["@Expired"].Value;

                _logger.LogInformation("The verfication link for AuthKey ({}) has returned an expired status of ({}).  ClientId={ClientId}", authKey, expired, comm.Parameters["@ClientId"].Value.ToString());

                return expired;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error fetching the API Key.");
                return true;
            }
        }

        /// <summary>
        /// Delete/deactivate the clients account.
        /// </summary>
        /// <param name="clientId"></param>
        //public async Task<string> DeleteClientAccount(double clientId)
        //{
        //    try
        //    {
        //        using SqlConnection conn = new SqlConnection(_creditSafeConfig.DolfinPaymentConnectionString);
        //        conn.Open();
        //        using SqlCommand comm = new SqlCommand(_creditSafeConfig.VerificationLinkExpiredStoredProcedure, conn);
        //        comm.CommandType = CommandType.StoredProcedure;
        //        comm.Parameters.Add("@ClientId", SqlDbType.Int).Direction = ParameterDirection.Output;
        //        await comm.ExecuteNonQueryAsync();

        //        _logger.LogInformation("Deleted the client account. ClientId={ClientId}", comm.Parameters["@ClientId"].Value.ToString());

        //        return string.Empty;

        //    }
        //    catch (SqlException ex)
        //    {
        //        _logger.LogError(ex, "Error deleting the account, error message ({}). ClientId={ClientId}", ex.Message, clientId);

        //        return ex.Message;
        //    }
        //}

        private static string TrimValue(string value)
        {
            if (value != null)
            {
                char[] charsToTrim = { ' ', '\n', '\r' };
                return value.Trim(charsToTrim);
            }
            else
            {
                return value;
            }
        }
    }
}