﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditSafeApi.Repositories.Interfaces
{
    public interface IGenericFunctions
    {
        public bool CheckKeyExists(Dictionary<string, object> result, string key) { return false; }
        public string GetValueFromDictionary(Dictionary<string, object> result, string key) { return string.Empty; }
        public async Task<string> CheckforDirectory(string filepath, double clientId) { return string.Empty; }
        public async Task<string> GeneratePdfFile(string type, double clientId, string filePath, string htmlString, string clientName, bool ftp, string RequestName) { return string.Empty; }
        public string Encrypt(string Text, string Key) { return string.Empty; }
        public string Decrypt(string EncryptedText, string Key) { return string.Empty; }
        public string GenerateEncryptionKey() { return string.Empty; }
    }
}
