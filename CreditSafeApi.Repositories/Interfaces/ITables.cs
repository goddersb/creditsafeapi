﻿using CreditSafeApi.Models.Api;
using System.Data;

namespace CreditSafeApi.Repositories.Interfaces
{
    public interface ITables
    {
         void CreatePersonColumns(DataTable tblPerson) { }
         void CreateAddressColumns(DataTable tblAddress) { }
         void CreateAliasColumns(DataTable tblAlias) { }
         void CreateArticlesColumns(DataTable tblArticles) { }
         void CreateSanctionsColumns(DataTable tblSanctions) { }
         void CreateNotesColumns(DataTable tblNotes) { }
         void CreateLinkedBusinessesColumns(DataTable tblLinkedBusinesses) { }
         void CreateLinkedPersonsColumns(DataTable tblLinkedPerson) { }
         void CreatePoliticalPositionsColumns(DataTable tblPoliticalPositions) { }
         void CreateBusinessColumns(DataTable TblBusiness) { }
         static void AddDataColumn(DataTable tbl, string name, string type) { }
        void PopulatePerson(PersonResponse response, DataTable tblPerson) { }
        void PopulateBusiness(BusinessResponse response, DataTable tblPerson) { }
        void PopulateAddresses(PersonResponse response, DataTable tblAddresses) { }
        void PopulateAlias(PersonResponse response, DataTable tblAlias) { }
        void PopulateArticles(PersonResponse response, DataTable tblArticles) { }
        void PopulateSanctions(PersonResponse response, DataTable tblSanctions) { }
        void PopulateNotes(PersonResponse response, DataTable tblNotes) { }
        void PopulateLinkedBusinesses(PersonResponse response, DataTable tblLinkedBusinesses) { }
        void PopulateLinkedPersons(PersonResponse response, DataTable tblLinkedPersons) { } 
        void PopulatePoliticalPositions(PersonResponse response, DataTable tblPoliticalPositions) { }
    }
}
