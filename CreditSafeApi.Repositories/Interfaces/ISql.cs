﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using CreditSafeApi.Models.Api;

namespace CreditSafeApi.Repositories.Interfaces
{
    public interface ISql
    {
        public async Task<bool> UpdateRiskAssessmentPersonTable(string Id, DataTable tblperson, DataTable tblAddresses, DataTable tblAlias,
                                               DataTable tblArticles, DataTable tblSanctions, DataTable tblNotes, DataTable tblLinkedBusinesses,
                                               DataTable tblLinkedPersons, DataTable tblPoliticalPositions) { return false; }

        public async Task<bool> UpdateRiskAssessmentBusinessTable(string Id, DataTable tblperson, DataTable tblAddresses, DataTable tblAlias,
                                              DataTable tblArticles, DataTable tblSanctions, DataTable tblNotes, DataTable tblLinkedBusinesses,
                                              DataTable tblLinkedPersons) { return false; }

        public async Task<ClientDetails> FetchClientDetails(double clientId) { return null; }
        public async Task<ClientDetails> FetchClientDetails(string email) { return null; }
        public async Task<Client> FetchClient(double clientId, bool hidePassord =false) { return null; }
        public async Task<Client> ValidateClient(string email, string password) { return null; }
        public async Task<bool> ValidClientId(double clientId) { return false; }
        public async Task<List<Countries>> FetchCountries() { return null; }
        public async Task<List<FilePaths>> FetchFilePaths() { return null; }
        public async Task<bool> UpdateClientDetails(ClientUpdateRequest clientUpdateRequest, string clientId) { return false; }
        public async Task<string> FetchApiKey(string userId) { return string.Empty; }
        public async Task<string> FetchAdminId() { return string.Empty; }
        public async Task<bool> InsertClientRequest(string clientId, string apiKey, string requestType, string requestStatus) { return false; }
        public async Task<ClientResponse> InsertClientDetails(ClientRequest clientRequest, double clientId) { return null; }
        public async Task<bool> InsertClientAuthDetails(string clientId, string authKey) { return false; }
        public async Task<ClientResponse> ValidateClientAuthKey(string authKey) { return null; }
        public async Task<bool> UpdateClientAuthKey(string clientId, string authKey) { return false; }
        public async Task<bool> CheckForExpiredLink(string authKey) { return false; }
        public async Task<string> DeleteClientAccount(double clientId) { return string.Empty; }
        public async Task<bool> InsertFtpFiles(double clientId, string ftpFilePath) { return false; }
    }
}
