﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System;
using System.Security.Claims;
using CreditSafeApi.Models.Configuration;
using CreditSafeApi.Repositories;

namespace CreditSafeApi.Services
{
    public class ApiKeyAuthHandler : AuthenticationHandler<ApiKeyAuthOpts>
    {
        private readonly CreditSafeConfig _creditSafeConfig;

        public ApiKeyAuthHandler(IOptionsMonitor<ApiKeyAuthOpts> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock,
                                                CreditSafeConfig creditSafeConfig)
            : base(options, logger, encoder, clock)
        {
            _creditSafeConfig = creditSafeConfig;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var sqlRepository = new SqlRepository(_creditSafeConfig, Logger);
            string apiKey;
            string authorization = Request.Headers["ApiKey"];
            var userId = Request.Query["ClientId"].ToString();
            string token = string.Empty;

            //Get the Controller Name.
            var controllerName = Request.Path.ToString().Replace("/api/v1/", "").Replace($"/{userId}", "");

            //Only authenticate Posts.
            if ((Request.Method == "POST" && !string.IsNullOrEmpty(userId)) || controllerName == "SafeSearch" || (Request.Method == "DELETE" && !string.IsNullOrEmpty(userId)))
            {
                //Check to see if the client Id is valid.
                if (!await sqlRepository.ValidClientId(userId))
                {
                    Logger.LogError("Authentication Error - invalid Client Id value. ClientId={ClientId}", userId);
                    return AuthenticateResult.Fail($"Authentication Error - invalid Client Id value ({userId})");
                }

                if (string.IsNullOrEmpty(authorization))
                {
                    Logger.LogError("Authentication Error - no API Key passed to the {} Endpoint. ClientId={ClientId}", controllerName, userId);
                    return AuthenticateResult.Fail("Authentication Error - no API Key passed to the Endpoint.");
                }
                else
                {
                    apiKey = await sqlRepository.FetchApiKey(userId);
                    token = authorization;
                }

                if (string.IsNullOrEmpty(token))
                {
                    Logger.LogError("Authntication Error - token not found ClientId={ClientId}", userId);
                    return AuthenticateResult.NoResult();
                }

                bool result = false;
                if (token.ToUpper() == (string.IsNullOrEmpty(apiKey) ? string.Empty : apiKey.ToUpper()))
                {
                    Logger.LogInformation("Successfully passed api authorisation. ClientId={ClientId}", userId);
                    result = true;
                }
                else
                {
                    result = false;
                }
                if (!result)
                {
                    Logger.LogError("Authentication Error - token not matched. ClientId={ClientId}", userId);
                    return AuthenticateResult.Fail($"token not matched ({apiKey}).");
                }
                else
                {
                    var id = new ClaimsIdentity(
                        new Claim[] { new Claim("Key", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(token))) },
                        Scheme.Name
                    );
                    ClaimsPrincipal principal = new ClaimsPrincipal(id);
                    var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(), Scheme.Name);
                    return AuthenticateResult.Success(ticket);
                }
            }
            else
            {
                var id = new ClaimsIdentity(
                       new Claim[] { new Claim("Key", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_creditSafeConfig.DefaultToken))) },
                       Scheme.Name
                   );
                ClaimsPrincipal principal = new ClaimsPrincipal(id);
                var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(), Scheme.Name);
                return AuthenticateResult.Success(ticket);
            }
        }
    }
}
