﻿using CreditSafeApi.Models.Api;
using CreditSafeApi.Models.Configuration;
using CreditSafeApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CreditSafeApi.Controllers
{
    [ApiController]
    [Route("api/v1/Client")]
    public class DashroClientController : Controller
    {
        private readonly CreditSafeConfig _creditSafeConfig;
        private readonly NrecoConfig _nrecoConfig;
        private readonly ExchangeConfig _exchangeConfig;
        private readonly FtpConfig _ftpConfig;
        private readonly ILogger<DashroClientController> _logger;

        public DashroClientController(CreditSafeConfig creditSafeConfig, NrecoConfig nrecoConfig, 
            ExchangeConfig exchangeConfig, FtpConfig ftpConfig, ILogger<DashroClientController> logger)
        {
            _creditSafeConfig = creditSafeConfig;
            _nrecoConfig = nrecoConfig;
            _exchangeConfig = exchangeConfig;
            _ftpConfig = ftpConfig;
            _logger = logger;
        }

        [HttpPut]
        public async Task<ActionResult>AddClient(ClientRequest clientRequest)
        {
            try
            {
                //Create new instances of the SqlRepository.
                var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

                //Create new instances of the SqlRepository.
                var emailRepository = new EmailRepository(_creditSafeConfig, _exchangeConfig,_nrecoConfig, _logger);

                //Create new instances of the GenericFunctionsRepository.
                var genericFunctionsRepository = new GenericFunctionsRepository(_creditSafeConfig, _nrecoConfig, _logger);

                //Validate the payload.
                var isValid = Validate(clientRequest, _creditSafeConfig);
                if (!string.IsNullOrEmpty(isValid))
                {
                    ResponseBody responseBody = new ResponseBody
                    {
                        Message = $"Validation error occurred - Message ({isValid})"
                    };
                    _logger.LogError($"Validation error occurred - Message ({isValid})");
                    return BadRequest(responseBody);
                }

                //Insert the new client details.
                ClientResponse clientResponse = await sqlRepository.InsertClientDetails(clientRequest);

                if (!(string.IsNullOrEmpty(clientResponse.ClientId) || string.IsNullOrEmpty(clientResponse.ApiKey)))
                {
                    //Fetch the encrypted auth key.
                    var ecpryptedLinkValue = genericFunctionsRepository.Encrypt(clientResponse.ApiKey, genericFunctionsRepository.GenerateEncryptionKey());

                    //Send the confirmation email.
                    var result = await emailRepository.SendAuthenticationEmail(clientRequest, clientResponse, ecpryptedLinkValue);

                    //Insert the client authentication details into the table DolfinPaymentApiClientAuthenticate.
                    if (result)
                    {
                        var inserted = await sqlRepository.InsertClientAuthDetails(clientResponse.ClientId, ecpryptedLinkValue);
                    }

                    return Ok(clientResponse);
                }
                else
                {
                    ResponseBody responseBody = new ResponseBody
                    {
                        Message = $"Errors occured attempting to insert the new client ({clientRequest.Forename} {clientRequest.Surname}), please contact Dashro Solutions for details."
                    };
                    return BadRequest(responseBody);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UpdateClient(ClientUpdateRequest clientUpdateRequest, [FromQuery] int clientId)
        {
            //Create new instances of the SqlRepository.
            var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);
            
            //Validate the Payload.
            var isValid = ValidateUpdate(clientUpdateRequest);
            if (!string.IsNullOrEmpty(isValid))
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"Validation error occurred - Message ({isValid})"
                };
                _logger.LogError("Validation error occurred - Message ({})  ClientId={ClientId}", isValid, clientId);
                return BadRequest(isValid);
            }

            try
            {
                var result = await sqlRepository.UpdateClientDetails(clientUpdateRequest, clientId);
                 if (result)
                {

                    _logger.LogInformation("Client details updated successfully, updated by ({}). ClientId={ClientId}", clientUpdateRequest.updatedBy, clientId);

                    ResponseBody responseBody = new ResponseBody
                    {
                        Message = $"Successfully updated the client details for client id {clientId}."
                    };
                    return Ok(responseBody);
                }
                 else
                {
                    ResponseBody responseBody = new ResponseBody
                    {
                        Message = $"Errors occured attempting to update the client detials for client id ({clientId}), please contact Dashro Solutions for details"
                    };
                    return BadRequest(responseBody);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error updating the client details for Client Id {}. ClientId={ClientId}", clientId, clientId);
                return BadRequest($"Error updating the client details for Client Id ({clientId}).");
            }
        }

        //[Authorize]
        //[HttpDelete]
        //public async Task<ActionResult> DeleteClientAccount([FromQuery] int clientId)
        //{
        //    //Create new instances of the SqlRepository.
        //    var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

        //    var result = await sqlRepository.DeleteClientAccount(clientId);
        //    if (string.IsNullOrEmpty(result))
        //    {
        //        return NoContent();
        //    }
        //    else
        //    {
        //        return BadRequest(result);
        //    }
        //}

        [HttpGet("Authenticate")]
        public async Task<ActionResult> AuthenticateClient([FromQuery] string authKey)
        {
            //Create new instances of the GenericFunctionsRepository.
            var genericFunctionsRepository = new GenericFunctionsRepository(_creditSafeConfig, _nrecoConfig, _logger);

            //Create new instances of the SqlRepository.
            var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            var linkExpired = await sqlRepository.CheckForExpiredLink(authKey);

            //Check to see if the activation link has expired.
            if (!linkExpired)
            {
                //Check to see if the key exists in the table?
                ClientResponse clientValidated = await sqlRepository.ValidateClientAuthKey(authKey);

                if (!string.IsNullOrEmpty(clientValidated.ClientId) && !clientValidated.Validated)
                {
                    //Set the clint auth details as validated.
                    await sqlRepository.UpdateClientAuthKey(clientValidated.ClientId, authKey);

                    //Fetch the client details once validation has passed.
                    ClientDetails clientDetails = await sqlRepository.FetchClientDetails(Convert.ToDouble(clientValidated.ClientId, CultureInfo.InvariantCulture));

                    //Create new instances of the SqlRepository.
                    var emailRepository = new EmailRepository(_creditSafeConfig, _exchangeConfig, _nrecoConfig, _logger);

                    //Create List of folders to create.
                    List<string> ftpFolders = new List<string>();
                    ftpFolders.Add($"{_ftpConfig.FtpUrl}{clientDetails.ClientId}");
                    ftpFolders.Add($"{_ftpConfig.FtpUrl}{clientDetails.ClientId}/{_ftpConfig.FtpClientFolder}");
                    ftpFolders.Add($"{_ftpConfig.FtpUrl}{clientDetails.ClientId}/{_ftpConfig.FtpServerFolder}");
                    ftpFolders.Add($"{_ftpConfig.FtpUrl}{clientDetails.ClientId}/{_ftpConfig.FtpProcessedFolder}");

                    //Create a new instance of the Ftp Repository.
                    var ftpRepository = new FtpRepository(_ftpConfig, _logger);

                    //Create the ftp folder structure for the new client.
                    foreach(string folder in ftpFolders)
                    {
                        ftpRepository.CreateClientFtpFolders(clientDetails, folder);
                    }

                    //Send email to client detailing their client id and Api Key.
                    await emailRepository.SendConfirmationEmail(clientDetails);

                    return Redirect($"{_creditSafeConfig.RedirectUrl}");
                }
                else if (string.IsNullOrEmpty(clientValidated.ClientId))
                {
                    ResponseBody responseBody = new ResponseBody
                    {
                        Message = "Account could not be located, please contact Dashro Solutions for more information."
                    };
                    _logger.LogInformation("Account could not be located for AuthKey ({}), please contact Dashro Solutions for more information.", authKey);
                    return BadRequest(responseBody);
                }
                else
                {
                    ResponseBody responseBody = new ResponseBody
                    {
                        Message = $"Account has already been verified for Client Id ({clientValidated.ClientId})."
                    };
                    _logger.LogInformation("Account has already been verified for Client Id ({}). ClientId={ClientId}", clientValidated.ClientId, clientValidated.ClientId);
                    return BadRequest(responseBody);
                }
            }
            else
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"The verification link has expired."
                };
                return BadRequest(responseBody);
            }
        }

        [HttpPost("ResendAuthentication")]
        public async Task<object> ResendAuthenticationEmail(ResendVerificationRequest resendVerificationRequest)
        {
            
            //Create new instances of the SqlRepository.
            var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            //Create new instances of the GenericFunctionsRepository.
            var genericFunctionsRepository = new GenericFunctionsRepository(_creditSafeConfig, _nrecoConfig, _logger);

            //Create new instances of the SqlRepository.
            var emailRepository = new EmailRepository(_creditSafeConfig, _exchangeConfig, _nrecoConfig, _logger);

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(resendVerificationRequest);

            if (clientDetails.Active)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"Account was activated on ({String.Format("{0:dd/MM/yyyy hh:mm tt}", clientDetails.AuthDate)}) and cannot be requested again!"
                };
                _logger.LogInformation("Account was activated on ({}) and cannot be requested again!. ClientId={ClientId}", String.Format("{0:dd/MM/yyyy hh:mm tt}", clientDetails.AuthDate), clientDetails.ClientId);
                return BadRequest(responseBody);
            }

            //Validate the Payload.
            if (resendVerificationRequest.Email.Length == 0)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"Email cannot be empty!"
                };
                _logger.LogInformation("Email cannot be empty!. ClientId={ClientId}", clientDetails.ClientId);
                return BadRequest(responseBody);
            }
            else if (!new EmailAddressAttribute().IsValid(resendVerificationRequest.Email))
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"Invalid email address {resendVerificationRequest.Email} used."
                };
                _logger.LogInformation("Invalid email address {} entered!. ClientId={ClientId}", resendVerificationRequest.Email, clientDetails.ClientId);
                return BadRequest(responseBody);
            }
        
            if (!(string.IsNullOrEmpty(clientDetails.ClientId)))
            {
                //Fetch the encrypted auth key.
                var ecpryptedLinkValue = genericFunctionsRepository.Encrypt(clientDetails.ApiKey, genericFunctionsRepository.GenerateEncryptionKey());

                //Send the confirmation email.
                var result = await emailRepository.SendAuthenticationEmail(clientDetails, ecpryptedLinkValue);

                //Insert the client authentication details into the table DolfinPaymentApiClientAuthenticate.
                if (result)
                {
                    bool inserted = await sqlRepository.InsertClientAuthDetails(clientDetails.ClientId, ecpryptedLinkValue);
                }

                return Redirect($"{_creditSafeConfig.RedirectUrl}");
            }

            return null;
        }

        [HttpGet]
        public async Task<object> GetClientDetails([FromQuery] int clientId)
        {
            //Create new instances of the SqlRepository.
            var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            try
            {
                ClientDetails clientDetails = await sqlRepository.FetchClient(clientId, false);

                if (clientDetails.ApiKey!=null)
                {
                    _logger.LogInformation("Client details retrieved successfully. ClientId={ClientId}", clientId);
                    return clientDetails;
                }
                else
                {
                    ResponseBody responseBody = new ResponseBody
                    {
                        Message = $"No client details located for the Client Id {clientId}."
                    };
                    _logger.LogInformation("No client details located for the Client Id {}. ClientId={ClientId}", clientId, clientId);
                    return BadRequest(responseBody);
                }
            }
            catch (Exception ex)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"Error fetching the client details for Client Id ({clientId})."
                };
                _logger.LogError(ex.Message, "Error fetching the client details for Client Id {}. ClientId={ClientId}", clientId, clientId);
                return BadRequest(responseBody);
            }
        }

        [HttpGet("Validate")]
        public async Task<object> ValidateClient([FromQuery] string email, string password)
        {
            StringBuilder errorString = new StringBuilder();

            //Create new instances of the SqlRepository.
            var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    errorString.Append("Email is missing");
                    
                }
                else
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        errorString.Append($"{(errorString.Length != 0 ? "; " : string.Empty)}Email {email} is incorrectly formatted");
                    }
                }

                if (string.IsNullOrEmpty(password))
                {
                    errorString.Append($"{(errorString.Length != 0 ? "; " : string.Empty)}Password is missing");
                }

                if(errorString.Length!=0)
                {

                    ValidateResponse validateResponse = new ValidateResponse
                    {
                        Message = errorString.ToString(),
                        ResponseCode = "400"
                    };

                    _logger.LogWarning("Client validation errors located {}.", errorString.ToString());

                    return BadRequest(validateResponse);
                }

                //Fetch the client details.
                Client client = await sqlRepository.ValidateClient(email);
                if (client.ApiKey == null)
                {
                    ValidateResponse validateResponse = new ValidateResponse
                    {
                        Message = $"No client details located for the email: {email}, password: {password}",
                        ResponseCode = "400"
                    };

                    _logger.LogWarning($"No client details located for the email: {email}, password: {password}");

                    return BadRequest(validateResponse);
                }
                
                if (client.Password == password)
                {
                    _logger.LogInformation("Client details retrieved successfully. ClientId={ClientId}", client.ClientId);
                    return (client);
                }
                else
                {
                    ValidateResponse validateResponse = new ValidateResponse
                    {
                        Message = $"The password does not match the one stored for the email: {email}.",
                        ResponseCode = "400"
                    };

                    _logger.LogWarning("The password does not match the one stored for the email: {}.  ClientId={ClientId}", email, client.ClientId);


                    return BadRequest(validateResponse);
                }

            }
            catch (Exception ex)
            {
                ValidateResponse validateResponse = new ValidateResponse
                {
                    Message = ex.Message,
                    ResponseCode = "400"
                };

                return BadRequest(validateResponse);
            }
        }

        /// <summary>
        /// Perform validation on the Post.
        /// </summary>
        /// <param name="clientUpdateRequest"></param>
        /// <returns></returns>
        private string ValidateUpdate(ClientUpdateRequest clientUpdateRequest)
        {
            if (string.IsNullOrEmpty(clientUpdateRequest.updatedBy))
            {
                var msg = $"The updated by field must contain a value!";
                return msg;
            }

            if (!string.IsNullOrEmpty(clientUpdateRequest.CompanyName))
            {
               if(clientUpdateRequest.CompanyName.Length==0)
                {
                    var msg = $"Company name cannot be empty!";
                    return msg;
                }
            }

            if (!string.IsNullOrEmpty(clientUpdateRequest.Website))
            {
                Uri uri = null;
                if (clientUpdateRequest.Website.Length == 0)
                {
                    var msg = $"Website cannot be empty!";
                    return msg;
                }
                else if (!Uri.TryCreate(clientUpdateRequest.Website, UriKind.RelativeOrAbsolute, out uri) || null == uri)
                {
                    var msg = $"The website {clientUpdateRequest.Website} is invalid, please correct then resubmit!";
                    return msg;
                }
            }

            if (!string.IsNullOrEmpty(clientUpdateRequest.Password))
            {
                var result = validatePassword(clientUpdateRequest.Password);
                if (!result)
                {
                    return "Passwords should be a minimumn of 8 characters, contain numbers 1-9, upper and lower characters and special characters such as !, @, # , $ , %, ^, &, *,?"; 
                }
            }

            if (!string.IsNullOrEmpty(clientUpdateRequest.Email))
            {
                if (clientUpdateRequest.Email.Length == 0 )
                {
                    var msg = $"Email cannot be empty!";
                    return msg;
                }
                else if (!new EmailAddressAttribute().IsValid(clientUpdateRequest.Email))
                {
                    var msg = $"Invalid email address {clientUpdateRequest.Email} used!";
                    return msg;
                }
            }

            //if (clientUpdateRequest.EndDate.ToString().Length > 0)
            //{
            //    if (DateTime.UtcNow > clientUpdateRequest.EndDate)
            //    {
            //        var msg = $"The end date ({String.Format("{0:dd/MM/yyyy}", clientUpdateRequest.EndDate)}) cannot be before the current date ({String.Format("{0:dd/MM/yyyy}", DateTime.UtcNow)})!";
            //        return msg;
            //    }
            //}
     
            return string.Empty;
        }

        /// <summary>
        /// Perform validation on the Put.
        /// </summary>
        /// <param name="clientRequest"></param>
        /// <returns></returns>
        private string Validate(ClientRequest clientRequest, CreditSafeConfig creditSafeConfig)
        {
            if (string.IsNullOrEmpty(clientRequest.Forename))
            {
                var msg = $"Client Foremane must be populated in order create a new client account!";
                return msg;
            }

            if (string.IsNullOrEmpty(clientRequest.Surname))
            {
                var msg = $"Client Surname must be populated in order create a new client account!";
                return msg;
            }

            if (string.IsNullOrEmpty(clientRequest.CompanyName))
            {
                var msg = $"Company Name must be populated in order create a new client account!";
                return msg;
            }

            if (!string.IsNullOrEmpty(clientRequest.Website))
            {
                Uri uri = null;
                if (!Uri.TryCreate(clientRequest.Website, UriKind.RelativeOrAbsolute, out uri) || null == uri)
                {
                    var msg = $"The website {clientRequest.Website} is invalid, please correct then resubmit!";
                    return msg;
                }
            }
            else
            {
                var msg = $"Website must be populated in order create a new client account!";
                return msg;
            }

            if (!string.IsNullOrEmpty(clientRequest.Email))
            {
                if (!new EmailAddressAttribute().IsValid(clientRequest.Email))
                {
                    var msg = $"Invalid email address {clientRequest.Email} used!";
                    return msg;
                }
            }
            else
            {
                var msg = $"Email address must be populated in order create a new client account!";
                return msg;
            }

            if (string.IsNullOrEmpty(clientRequest.Password))
            {
                var msg = $"A valid password must be populated in order create a new client account!";
                return msg;
            }
            else
            {
                var result = validatePassword(clientRequest.Password);
                if (!result)
                {
                    return "Passwords should be between 8 - 15 characters, contain numbers 1-9, upper and lower characters and special characters such as !, @, # , $ , %, ^, &, *,?";
                }
            }

            //if (clientRequest.EndDate == null)
            //{
            //    var msg = "The end date is required in order to create a new client account!";
            //    return msg;
            //}
            //else if (clientRequest.EndDate != null)
            //{
            //    if (DateTime.UtcNow > clientRequest.EndDate)
            //    {
            //        var msg = $"The end date ({String.Format("{0:dd/MM/yyyy}", clientRequest.EndDate)}) cannot be before or be the same as the current date ({String.Format("{0:dd/MM/yyyy}", DateTime.UtcNow)})!";
            //        return msg;
            //    }
            //}

            //Will need to rework this code when the policy for number of requests is defined.
            if (clientRequest.SearchAllocation==null)
            {
                clientRequest.SearchAllocation = creditSafeConfig.DefaultSearchAllocation;
            }

            if (clientRequest.SwiftAllocation == null)
            {
                clientRequest.SwiftAllocation = creditSafeConfig.DefaultSwiftAllocation;
            }

            return string.Empty;
        }

        private bool validatePassword(string password)
        {
            const int MIN_LENGTH = 8;

            if (password == null) throw new ArgumentNullException();

            bool meetsLengthRequirements = password.Length >= MIN_LENGTH;
            bool hasUpperCaseLetter = false;
            bool hasLowerCaseLetter = false;
            bool hasDecimalDigit = false;

            if (meetsLengthRequirements)
            {
                foreach (char c in password)
                {
                    if (char.IsUpper(c)) hasUpperCaseLetter = true;
                    else if (char.IsLower(c)) hasLowerCaseLetter = true;
                    else if (char.IsDigit(c)) hasDecimalDigit = true;
                }
            }

            bool isValid = meetsLengthRequirements
                        && hasUpperCaseLetter
                        && hasLowerCaseLetter
                        && hasDecimalDigit
                        ;
            return isValid;
        }

        public static PasswordScore CheckStrength(string password)
        {
            int score = 1;
            if (password.Length < 1)
                return PasswordScore.Blank;
            if (password.Length < 4)
                return PasswordScore.VeryWeak;
            if (password.Length >= 8) score++;
            if (password.Length >= 12) score++;
            if (Regex.IsMatch(password, @"[0-9]+(\.[0-9][0-9]?)?"))   //number only //"^\d+$" if you need to match more than one digit.
                score++;
            if (Regex.IsMatch(password, @"^(?=.*[a-z])(?=.*[A-Z]).+$")) //both, lower and upper case
                score++;
            if (Regex.IsMatch(password, @"[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]")) //^[A-Z]+$
                score++;
            return (PasswordScore)score;
        }
    }
}
